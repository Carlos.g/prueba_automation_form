from selenium.webdriver.common.by import By

class LoginPageLocators:
    USERNAME_INPUT = (By.ID, "txt-username")
    PASSWORD_INPUT = (By.ID, "txt-password")
    SUBMIT_BTN = (By.ID, "btn-login")
