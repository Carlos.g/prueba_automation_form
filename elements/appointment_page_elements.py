from selenium.webdriver.common.by import By

class AppointmentPageLocators:
    FACILITY_SELECT = (By.ID, "combo_facility")
    HOSPITAL_READMISSION_CHECK = (By.ID, "chk_hospotal_readmission")
    MEDICARE_RADIO = (By.ID, "radio_program_medicare")
    MEDICAID_RADIO = (By.ID, "radio_program_medicaid")
    NONE_RADIO = (By.ID, "radio_program_none")
    VISIT_DATE_INPUT = (By.ID, "txt_visit_date")
    COMMENT_AREA_INPUT = (By.ID, "txt_comment")
    SUBMIT_APPOINTMENT_BTN = (By.ID, "btn-book-appointment")
    APPOINTMENT_CONFIRMATION = (By.XPATH, "//h2[contains(text(),'Appointment Confirmation')]")