from .base_test import BaseTest
from pages.main_page import MainPage as mp


class test_login(BaseTest):


    def test_login_valid(self):
        #inititalize page
        username = "John Doe"
        password = "ThisIsNotAPassword"

        # perform Login
        self.login_with_credentials(username, password)
        url = self.get_url()
        self.assertIn("appointment", url, "Appointment page is not current page")




