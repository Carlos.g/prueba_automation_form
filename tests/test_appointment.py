from .base_test import BaseTest
from pages.appointment_page import AppointmentPage


class test_appointment(BaseTest):


    def test_appointment_valid(self):
        #login
        username = "John Doe"
        password = "ThisIsNotAPassword"
        self.login_with_credentials(username, password)

        #initialize form data
        appointment_page = AppointmentPage(self.driver)
        select_index = 2
        visit_date = "15/01/2024"
        comment_text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,"
        correct_appointment_message = "Appointment Confirmation"

        #Complete Form
        appointment_page.set_facility_select_by_index(select_index)
        appointment_page.set_check_readmission()
        appointment_page.set_radio_option()
        appointment_page.enter_date(visit_date)
        appointment_page.enter_comment(comment_text)
        appointment_page.click_submit()
        confirmation_text = appointment_page.find_confirmation_text()

        #assertion
        self.assertEqual(correct_appointment_message, confirmation_text, "Appointment was not created")








