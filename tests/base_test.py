import unittest

from selenium import webdriver
from pages.login_page import LoginPage
from pages.main_page import MainPage



class BaseTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.login_page = LoginPage(self.driver)
        self.main_page = MainPage(self.driver)
        # navigate to main page and click
        self.driver.get("https://katalon-demo-cura.herokuapp.com/")
        self.main_page.click_makeAppointment_btn()

    def tearDown(self):
        self.driver.quit()

    def login_with_credentials(self, username, password):
        self.login_page.enter_username(username)
        self.login_page.enter_password(password)
        self.login_page.click_login_button()

    def get_url(self):
        return self.driver.current_url
