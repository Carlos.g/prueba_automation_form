from pages.base_page import BasePage as bp
from elements.main_page_elements import MainPageLocators as mpl

class MainPage(bp):

    def click_makeAppointment_btn(self):
        makeAp_btn = self.wait_element(mpl.MAKE_APPOINTMENT_BTN)
        makeAp_btn.click()



