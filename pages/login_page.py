from .base_page import BasePage as bp
from elements.login_page_elements import LoginPageLocators as lpl

class LoginPage(bp):

    def enter_username(self, username):
        username_input = self.wait_element(lpl.USERNAME_INPUT)
        username_input.clear()
        username_input.send_keys(username)


    def enter_password(self, password):
        password_input = self.wait_element(lpl.PASSWORD_INPUT)
        password_input.clear()
        password_input.send_keys(password)


    def click_login_button(self):
        login_button = self.wait_element(lpl.SUBMIT_BTN)
        login_button.click()


