from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

class BasePage:

    def __init__(self, driver: webdriver):
        self.driver = driver



    def wait_element(self, locator):
        return WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locator)
        )

    def get_element_text(self, locator):
        return self.wait_element(locator).text

    def create_select(self, locator):
        return Select(self.wait_element(locator))
