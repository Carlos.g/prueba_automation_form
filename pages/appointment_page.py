from .base_page import BasePage
from elements.appointment_page_elements import AppointmentPageLocators as apl


class AppointmentPage(BasePage):

    def set_facility_select_by_index(self, i):
        select_input = self.create_select(apl.FACILITY_SELECT)
        select_input.select_by_index(i)


    def set_check_readmission(self):
        check_input = self.wait_element(apl.HOSPITAL_READMISSION_CHECK)
        check_input.click()


    def set_radio_option(self):
        login_button = self.wait_element(apl.NONE_RADIO)
        login_button.click()

    def enter_date(self, date):
        visit_date_input = self.wait_element(apl.VISIT_DATE_INPUT)
        visit_date_input.clear()
        visit_date_input.send_keys(date)

    def enter_comment(self, comment):
        comment_input = self.wait_element(apl.COMMENT_AREA_INPUT)
        comment_input.clear()
        comment_input.send_keys(comment)

    def click_submit(self):
        submit_btn = self.wait_element(apl.SUBMIT_APPOINTMENT_BTN)
        submit_btn.click()

    def find_confirmation_text(self):
        return self.get_element_text(apl.APPOINTMENT_CONFIRMATION)



